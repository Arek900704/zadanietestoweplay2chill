﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;
using Dummiesman;
using TouchScript.Layers;

public class LogicClass : MonoBehaviour
{

    public string modelsPath;
    public string screenshotPath;
    public GameObject rootObject;
    private int activeModel;

    public int captureWidth = 1920;
    public int captureHeight = 1080;

    public enum Format { JPG, PNG };
    public Format format = Format.PNG;

   
    private Rect rect;
    private RenderTexture renderTexture;
    private Texture2D screenShot;


    public void loadModel(string filePath)
    {
        GameObject model = null;

        if (File.Exists(filePath))
        {
            model = new OBJLoader().Load(filePath);
            model.transform.SetParent(rootObject.transform);
            model.AddComponent<TouchScript.Gestures.PanGesture>();
            model.AddComponent<TouchScript.Gestures.ScaleGesture>();
            RotateModel rotateScript =  model.AddComponent<RotateModel>();
            rotateScript.xSpeed = -1000;
            rotateScript.ySpeed = 1000;
            rotateScript.zoom = 1.0f;
            rotateScript.minZoom = 0.5f;
            rotateScript.maxZoom = 2.5f;
            rotateScript.zoomSpeed = -50;
            model.AddComponent<FullscreenLayer>();
            model.transform.localPosition = new Vector3(0, 0, 0);
            //model.AddComponent<BoxCollider>();
        }
    }
    string GeneratePath(string folder)
    {
        Random.seed = System.Environment.TickCount;
        string path = Application.dataPath.Replace("Assets", folder).Replace("FotoBudka3D_Data", folder);
        Regex regex = new Regex(@"[\w-]+_Data");
        return regex.Replace(path, folder);
    }
    

    void Start()
    {
        string inputpath = GeneratePath(modelsPath);  
        foreach (string file in System.IO.Directory.GetFiles(inputpath))
        {
            
            if(!file.EndsWith("mtl"))
            loadModel(file);
        }
        if(rootObject.transform.childCount>1)
        {
            for (int i = 1; i < rootObject.transform.childCount; i++)
            {
                rootObject.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
        activeModel = 0;

        rect = new Rect(0, 0, captureWidth, captureHeight);
        renderTexture = new RenderTexture(captureWidth, captureHeight, 24);
        screenShot = new Texture2D(captureWidth, captureHeight, TextureFormat.RGB24, false);

    }

    public void SavePicture()
    {
        Camera cameraScript = Camera.main.GetComponent<Camera>();
        cameraScript.targetTexture = renderTexture;
        cameraScript.Render();

        RenderTexture.active = renderTexture;
        screenShot.ReadPixels(rect, 0, 0);

        cameraScript.targetTexture = null;
        RenderTexture.active = null;

        byte[] fileData = null;

        if (format == Format.PNG)
        {
            fileData = screenShot.EncodeToPNG();
        }
        else if (format == Format.JPG)
        {
            fileData = screenShot.EncodeToJPG();
        }

        string outputPath = GeneratePath(screenshotPath);
        System.IO.Directory.CreateDirectory(outputPath);

        string filename = string.Concat(outputPath, "/", System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), ".", format.ToString().ToLower());

        

        new System.Threading.Thread(() =>
        {
            // create file and write optional header with image bytes
            var f = System.IO.File.Create(filename);
            f.Write(fileData, 0, fileData.Length);
            f.Close();
        }).Start();
    }

    public void nextModel()
    {
        if(activeModel<rootObject.transform.childCount-1)
        {
            rootObject.transform.GetChild(activeModel).gameObject.SetActive(false);
            activeModel++;
            rootObject.transform.GetChild(activeModel).gameObject.SetActive(true);
        }
    }

    public void previousModel()
    {
        if (activeModel > 0)
        {
            rootObject.transform.GetChild(activeModel).gameObject.SetActive(false);
            activeModel--;
            rootObject.transform.GetChild(activeModel).gameObject.SetActive(true);
        }
    }

}
