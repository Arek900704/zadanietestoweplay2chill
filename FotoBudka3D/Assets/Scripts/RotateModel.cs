﻿using System;
using TouchScript;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.UI;

public class RotateModel : MonoBehaviour {

    public Transform target;
    public Vector3 targetOffset;
    public float zoom = 5.0f;
    public float maxZoom = 20;
    public float minZoom = 5f;
    public float xSpeed = 15f;
    public float ySpeed = 40f;
    public int yMinLimit = -90;
    public int yMaxLimit = 90;
    public int zoomSpeed = 50;
    public float zoomDampening = 3.0f;
    public bool Enabled = true;
    public Slider zoomSlider;
    //------------------------------------------------------------
    private float xDeg = 0.0f;
    private float yDeg = 0.0f;
    private float currentDistance;
    private float desiredDistance;
    private Quaternion currentRotation;
    private Quaternion desiredRotation;
    private Quaternion rotation;
    private Vector3 position;
    private Vector3 touchDeltaPosition;
    private float Dummy;

    void Start()
    {
        //If there is no target, create a temporary target at 'distance' from the cameras current viewpoint
        if (!target)
        {
            GameObject go = new GameObject("Cam Target");
            go.transform.position = transform.position + (transform.forward * zoom);
            target = go.transform;
        }
        zoom = Vector3.Distance(transform.position, target.position);
        zoomSlider = GameObject.FindGameObjectWithTag("ZoomSlider").GetComponent<Slider>();
    
        currentDistance = zoom;
        desiredDistance = zoom;
        //be sure to grab the current rotations as starting points.
        position = transform.position;
        rotation = transform.rotation;
        currentRotation = transform.rotation;
        desiredRotation = transform.rotation;
        xDeg = Vector3.Angle(Vector3.forward, transform.forward);
        yDeg = Vector3.Angle(Vector3.up, transform.up);
        Dummy = zoom;
        
	}
	
	
	private void OnEnable ()
	{
		
		
		if (GetComponent<PanGesture> () != null) {
			GetComponent<PanGesture> ().StateChanged += onPanStateChanged;
		}
		if (GetComponent<ScaleGesture> () != null) {
			GetComponent<ScaleGesture> ().StateChanged += onScaleStateChanged;
		}
		

		xDeg = transform.rotation.eulerAngles.y;
		yDeg = transform.rotation.eulerAngles.x;
	}
	
	private void OnDisable ()
	{
		
		
		if (GetComponent<PanGesture> () != null) {
			GetComponent<PanGesture> ().StateChanged -= onPanStateChanged;
		}
		if (GetComponent<ScaleGesture> () != null) {
			GetComponent<ScaleGesture> ().StateChanged -= onScaleStateChanged;
		}
		
		
//		xDeg = currentRotation.eulerAngles.y;
//		yDeg = currentRotation.eulerAngles.x;
	}
	
	
	
   
	private void onPanStateChanged (object sender, GestureStateChangeEventArgs e)
	{
		if (!Enabled) {
			return;
		}
		switch (e.State) {
		case Gesture.GestureState.Began:
		case Gesture.GestureState.Changed:
			var gesture = (PanGesture)sender;
			touchDeltaPosition = gesture.LocalDeltaPosition;
			if (gesture.ActiveTouches.Count != 1) {
				return;
			}
			xDeg += touchDeltaPosition.x * xSpeed * 0.02f;
			yDeg -= touchDeltaPosition.y * ySpeed*0.02f;
                //Debug.Log(yDeg);	
			break;
		}
	}
	
	private void onScaleStateChanged (object sender, GestureStateChangeEventArgs e)
	{
		
		if (!Enabled) {
			return;
		}
		switch (e.State) {
		case Gesture.GestureState.Began:
		case Gesture.GestureState.Changed:
			var gesture = (ScaleGesture)sender;
			if (Math.Abs (gesture.LocalDeltaScale - 1) > 0.00001) {
				float speed = (gesture.LocalDeltaScale - 1);
				zoom = speed;
				if (zoom > maxZoom) {
					desiredDistance += zoom * Time.deltaTime * zoomSpeed * Mathf.Abs (desiredDistance);
				}
				if (zoom < minZoom) {
					desiredDistance -= zoom * Time.deltaTime * zoomSpeed * Mathf.Abs (desiredDistance);
				}
			}
			break;
		}
	}



    void LateUpdate ()
	{
		
			yDeg = ClampAngle (yDeg, yMinLimit, yMaxLimit);
		
        
            // set camera rotation 
         desiredRotation = Quaternion.Euler(yDeg, xDeg,0 );
        currentRotation = transform.rotation;
            rotation = Quaternion.Lerp(currentRotation, desiredRotation, Time.deltaTime * zoomDampening);
            transform.rotation = rotation;
        
		
		desiredDistance = Mathf.Clamp (desiredDistance, minZoom, maxZoom);
		currentDistance = Mathf.Lerp (currentDistance, desiredDistance, Time.deltaTime * zoomDampening);

        transform.localScale = new Vector3(zoomSlider.value, zoomSlider.value, zoomSlider.value);
        //transform.localScale = new Vector3(currentDistance,currentDistance, currentDistance);
    }

   

    private static float ClampAngle (float angle, float min, float max)
	{
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;
        //Debug.Log("Clamp:" + Mathf.Clamp(angle, min, max));
		return Mathf.Clamp (angle, min, max);
	}
}
